#include <iostream>
#include "De.h"
#include "Plateau.h"
#include <time.h>

using namespace std;


int main(int argc, char const *argv[]) {
  /* initialize random seed: */
  srand (time(NULL));  

  Plateau plateau;

  plateau.tourneDes();
  plateau.getValeur(1);
  plateau.bloquerDe(1);
  for(int j = 0; j < 100; j++) {
    plateau.tourneDes();
    for(int i = 0; i < 3; i++) {
      cout << "De[" << (i+1) << "] :" << plateau.getValeur(i) << endl;
    }
  }
  cout << plateau.nbPoints() << endl;
  
  return 0;
}